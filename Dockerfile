FROM debian:latest

# Software installation
RUN apt-get update
RUN apt-get install -y apache2 apache2-dev build-essential python3 python3-dev python3-pip wget
RUN pip3 install Flask
RUN mkdir mod_wsgi
RUN wget https://github.com/GrahamDumpleton/mod_wsgi/archive/4.6.5.tar.gz -O - | tar xvz --strip-components=1 -C /mod_wsgi
WORKDIR mod_wsgi
RUN ./configure --with-python=/usr/bin/python3 && make && make install
WORKDIR /
RUN rm -rf /mod_wsgi

# Apache configuration
RUN mkdir /ca
RUN mkdir /cert
RUN mkdir /conf
RUN mkdir /app
RUN mkdir /static
RUN mkdir /info
COPY wsgi.conf /etc/apache2/sites-available/wsgi.conf
RUN a2enmod headers
RUN a2enmod http2
RUN a2enmod ssl
RUN a2enmod proxy
RUN a2enmod proxy_connect
RUN a2ensite wsgi.conf

# CA key+certificate+config
#  could be overwritten by volumes:
#   /ca/cakey.pem
#   /ca/cacert.pem
RUN openssl genrsa > /ca/cakey.pem
RUN openssl req -new -x509 -key /ca/cakey.pem -days 365 -subj "/C=FR/CN=testCA/OU=-" -addext keyUsage=critical,keyCertSign > /ca/cacert.pem
COPY ca.cnf /ca/ca.cnf
RUN touch /ca/cadb.txt


# Default application
#  should be overwritten by a volume:
#    /app/app.py
RUN echo 'from app import application' > /conf/app.wsgi
COPY defaultapp.py /app/app.py


COPY entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 80 443
