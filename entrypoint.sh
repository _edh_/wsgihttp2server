#!/bin/bash
set -e

# Generate certificate - signed by CA in /ca
openssl genrsa > /cert/key.pem 2>/dev/null
openssl req -new -key /cert/key.pem -subj "/C=FR/CN=$(hostname)/OU=-" -addext "subjectAltName=DNS:$(hostname)" > /tmp/csr.pem 2>/dev/null
openssl ca -config /ca/ca.cnf -batch -in /tmp/csr.pem -cert /ca/cacert.pem -keyfile /ca/cakey.pem -create_serial -out /cert/cert.pem -notext 2>/dev/null
cat /cert/cert.pem /ca/cacert.pem > /cert/bundlecerts.pem

# Generate info page with content of certificates
cat << EOF > /info/info.html
<html>
 <ul>
  <li>
   <b>hostname</b>: <code>`hostname`</code>
  </li>
  <li>
   <b>cert</b>:
   <pre>`openssl x509 -in /cert/cert.pem -noout -text`</pre>
   <pre>`cat /cert/cert.pem`</pre>
  </li>
  <li>
   <b>CA</b>:
   <pre>`openssl x509 -in /ca/cacert.pem -noout -text`</pre>
   <pre>`cat /ca/cacert.pem`</pre>
  </li>
 </ul>
</html>
EOF

# Serve...
exec /usr/sbin/apache2ctl -D FOREGROUND

