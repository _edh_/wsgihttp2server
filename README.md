# WSGI server

## Build Docker

`docker build . -t wsgi`

## Develop server logic

edit a file called app.py which define a WSGI object called "application"

for example:

```
def application(environ, start_response):
    start_response('200 OK', [('Content-type', 'text/plain')])
    return [b'Hello World!']
```

or with Flask:

```
from flask import Flask
application = Flask("test")
@application.route("/")
def hello():
	return "Hello World!"
```

## Run

```
docker run -d --rm -p 80:80 -p 443:443 -v $(pwd)/app.py:/app/app.py --hostname `hostname` wsgi
```

### options

- add `-v $(pwd):/app/static` to previous command in order to serve static files
- run `docker logs <docker instance> -f` to have Apache logs

## Test

```
$ curl -v -k https://`hostname`/info --http2
$ curl -k https://`hostname` --http2
```

