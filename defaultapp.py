from flask import Flask
from flask import request
application = Flask("test")
@application.route("/", methods = ['GET', 'POST', 'DELETE'])
def hello():
    return f"hello {request.method}\n"
